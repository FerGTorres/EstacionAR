class comercioAdherido 
{
    constructor(id , nombre, direccion, horario , ubicacion )
	{
        this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.horarioAtencion = horario;
		this.ubicacion = ubicacion;
    }
}


class Ubicacion {
    constructor(lat, lon) {
        this.lat = lat;
        this.lon = lon;
    }
}

class Estacionamiento 
{
    constructor(id , direccion, ubicacion) 
	{
        this.id = id;
		this.direccion = direccion;
		this.ubicacion = ubicacion;
    }
}

class Zona 
{
    constructor(color, coordenadas) 
	{
        this.color = color;
        this.coordenadasDeCobertura = coordenadas;
    }	
	
	getColor()
	{
		return this.color;
	}
	
	getCoordenadasDeCobertura()
	{
		return this.coordenadasDeCobertura;
	}
}

var zonaVerde;
var zonaAzul;
var comerciosAdheridos; //array de comercioAdherido
var estacionamientos; //array de Estacionamiento

var map;
var EstacionamientosLayer;
var ComerciosLayer;
var comercios_cluster;
var estacionamiento_Cluster;
var estadiaMarker;
var overlays;

function onload()
{	
	cargarInstanciasLocales(); // se generan instancias locales de las clases
	cargarEstilos();
	inicializarMapa();
    this.comercios_cluster.addTo(this.map);
	this.estacionamiento_Cluster.addTo(this.map);
	
	this.overlays = 
		{
		"Estacionamientos": this.EstacionamientosLayer.addTo(this.map),
		"Comercios": this.ComerciosLayer.addTo(map),
		"Mi estadía actal" : this.estadiaMarker
		};
	L.control.layers(null, this.overlays).addTo(this.map);
};

function inicializarMapa ()
{
	this.EstacionamientosLayer = L.layerGroup();
	this.ComerciosLayer = L.layerGroup();
	
	let puntoDeMapa = [ -34.5427, -58.7126 ];
	this.map = L.map('mapid').setView(puntoDeMapa,16);
  
  	this.zonaAzul.forEach(seccion => 
	{
		cargarPoligono(seccion);
    });

	cargarPoligono(this.zonaVerde);

	
	cargarClusterDeComercios (this.comerciosAdheridos);
	cargarClusterDeEstacionamientos(this.estacionamientos);
	
    this.estadiaMarker=L.marker([-34.5427, -58.7126]);
    this.estadiaMarker.addTo(map).bindPopup('Av. Dr. Ricardo Balbín 1349').openPopup();
	
	setearFocusEnComercios();
	
	setearControlDeLayers();
	
	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'}).addTo(this.map);
}

function cargarPoligono (zona)
{
	L.polygon(zona.getCoordenadasDeCobertura() , {color: zona.getColor()}).addTo(this.map);
}

function cargarClusterDeComercios (comercios)
{
	this.comercios_cluster = L.markerClusterGroup();
	let marcadores = [];
	comercios.forEach(comercio => 
	{
        marcadores.push( L.marker( comercio.ubicacion, {icon: orangeIcon}).bindPopup("<p>" + comercio.nombre + "<br>" + comercio.direccion + "<br>" + comercio.horarioAtencion +  "</p>" ));
    });
	this.comercios_cluster.addLayers(marcadores).addTo(this.ComerciosLayer);
}

function cargarClusterDeEstacionamientos (estacionamientos)
{
	this.estacionamiento_Cluster = L.markerClusterGroup();
	let marcadores = [];
	estacionamientos.forEach(estacionamiento => 
	{
        marcadores.push( L.marker( estacionamiento.ubicacion, {icon: redIcon}).bindPopup("<p>" + estacionamiento.direccion + "</p>" ));
    });
	this.estacionamiento_Cluster.addLayers(marcadores).addTo(this.EstacionamientosLayer);
}
     
function setearFocusEnComercios()
{
	document.getElementById("verComercio1").onclick = function () {mostrarEnMapa("C1")};
	document.getElementById("verComercio2").onclick = function () {mostrarEnMapa("C2")};
	document.getElementById("verComercio3").onclick = function () {mostrarEnMapa("C3")};
	document.getElementById("verComercio4").onclick = function () {mostrarEnMapa("C4")};
	document.getElementById("verComercio5").onclick = function () {mostrarEnMapa("C5")};
}

function  mostrarEnMapa (idPedida)
{
	
	this.comerciosAdheridos.forEach(comercio => 
	{
		if(comercio.id===idPedida)
		{
			 map.setView(comercio.ubicacion,20);
			
			 popup = L.popup().setLatLng(comercio.ubicacion).setContent(comercio.nombre + "<br>" + comercio.direccion + "<br>" + comercio.horarioAtencion +  "</p>" ).openOn(map);
		}	
          });
}

function setearControlDeLayers()
{
	map.on('overlayremove', onOverlayRemove);
function onOverlayRemove(e)
	{
	   if (e.name === 'Comercios'){	
		document.getElementById("mapid").style.width = "100%"; 
		document.getElementById("right").style.width = "0%"; 
		document.getElementById("right").style.visibility = "collapse";
		} 	
	}


map.on('overlayadd', onOverlayAdd);
function onOverlayAdd(e)
		{
		if (e.name === 'Comercios')
			{
			document.getElementById("mapid").style.width = "75%"; 
			document.getElementById("right").style.width = "25%"; 
			document.getElementById("right").style.visibility = "visible"; 
			}
			
		if (e.name === "Mi estadía actal")
			{
			map.setView([-34.5427, -58.7126],20),
			popup = L.popup().setLatLng([-34.5427, -58.7126]).setContent('Av. Dr. Ricardo Balbín 1349').openOn(map);
			
			}
			
			
		}
	
	
}

//metodos para instancias

function cargarInstanciasLocales()
{	
	this.comerciosAdheridos = instanciarComerciosAdheridos(); 
	this.estacionamientos = instanciarEstacionamientos();
	instanciarZonas();	
}

function instanciarComerciosAdheridos()
{
	let lista = [new comercioAdherido ("C1", "Kiosko Sarmiento","Av. Dr. Ricardo Balbín 1349" ,   "de 8 a 20hs", new Ubicacion ( -34.543342599999995, -58.7131701)) ];
	lista.push(new comercioAdherido ("C2", "Cibercafe conecta2","Av. Dr. Ricardo Balbín 999" , "de 8 a 20hs", new Ubicacion ( -34.5405552, -58.7102663)) );
	lista.push(new comercioAdherido ("C3", "Supermercado Peko2","Av. Dr. Ricardo Balbín 1210" ,"de 8 a 20hs", new Ubicacion (-34.5424012, -58.7123661 )) );
	lista.push(new comercioAdherido ("C4", "Kiosko 24hs","Leandro N. Alem 1663" ,"de 8 a 20hs", new Ubicacion (-34.542967, -58.7143204 )) );
	lista.push(new comercioAdherido ("C5", "Farmacia La Via", "Domingo Faustino Sarmiento 1702" ,"de 8 a 20hs", new Ubicacion ( -34.541675399999995, -58.7145588)) );	
	
	return lista;
}

function instanciarEstacionamientos() 
{
	let lista = [new Estacionamiento("E0", "Peron 1500", new Ubicacion ( -34.5415359, -58.713831899999995)) ];
	lista.push( new Estacionamiento("E1", "Concejar Tribulato 1520", new Ubicacion (   -34.5415359, -58.713831899999995   )) );
	lista.push( new Estacionamiento("E2", "Juan Domingo Peron 1700", new Ubicacion (   -34.541694,-58.712814   )) );	
	lista.push( new Estacionamiento("E3", "Concejar Tribulato 100", new Ubicacion (  -34.542415,-58.713665    )) );	
	lista.push( new Estacionamiento("E4", "Juan Domingo Peron 1600", new Ubicacion ( -34.542318,-58.711855    )) );	
	lista.push( new Estacionamiento("E5", "Faustino Sarmiento 1600", new Ubicacion (  -34.543145,-58.712584   )) );
	lista.push( new Estacionamiento("E6", "Faustino Sarmiento 1550", new Ubicacion (    -34.543601,-58.711943  )) );
	lista.push( new Estacionamiento("E7", "Belgrano 1180", new Ubicacion (  -34.543364,-58.711315   )) );	
	lista.push( new Estacionamiento("E8", "Faustino Sarmiento 1500", new Ubicacion (   -34.544082,-58.711372   )) );	
	lista.push( new Estacionamiento("E9", "Sarmiento Gral Sarmiento", new Ubicacion (  -34.544550,-58.710761   )) );	
	lista.push( new Estacionamiento("E10", "Juan Domingo Peron 1103", new Ubicacion (  -34.543654,-58.709975   )) );
	
	lista.push( new Estacionamiento("E", "España 1200-1168", new Ubicacion (  -34.540080,-58.714824   )) );
	lista.push( new Estacionamiento("E", "Leandro N. Alem 1790", new Ubicacion (   -34.541548,-58.716250   )) );	
	lista.push( new Estacionamiento("E", "Cnel. Manuel Fraga 1786", new Ubicacion (  -34.542738,-58.716516   )) );	
	lista.push( new Estacionamiento("E", "Concejal Tribulato 1388", new Ubicacion (   -34.543781,-58.715225   )) );	
	lista.push( new Estacionamiento("E", "Peluffo 1600", new Ubicacion (  -34.544801,-58.713744   )) );
	lista.push( new Estacionamiento("E", "'Leandro N. Alem 1743", new Ubicacion (  -34.542245,-58.715532    )) );
	lista.push( new Estacionamiento("E", "Paunero 1799", new Ubicacion (   -34.540032,-58.713239  )) );	
	lista.push( new Estacionamiento("E", "Paunero 1648", new Ubicacion (   -34.540979,-58.712000   )) );	
	lista.push( new Estacionamiento("E", "Maestro Ángel DElía 1468", new Ubicacion (   -34.541830,-58.709062   )) );	
	lista.push( new Estacionamiento("E", "Paunero 1663", new Ubicacion (  -34.542361,-58.710070   )) );
	
	return lista;
}

function instanciarZonas()
{
this.zonaVerde = new Zona 
	( 
	"green",
	[ 
	new Ubicacion(-34.541129, -58.713552),
	new Ubicacion(-34.541931, -58.714422),
	new Ubicacion(-34.544664, -58.710663), 
	new Ubicacion(-34.543781, -58.709836),
	new Ubicacion(-34.541129, -58.713552),
	]
	);
	
	this.zonaAzul =  [
	new Zona 
	 ( 
	 "blue",
	 [ 
	new Ubicacion(-34.538690, -58.713434),
		new Ubicacion(-34.543175, -58.707253), 
		new Ubicacion(-34.544977, -58.709127), 
		new Ubicacion(-34.544169, -58.710247), 
		new Ubicacion(-34.543781, -58.709836), 
		new Ubicacion(-34.541129, -58.713552),
		new Ubicacion(-34.541501, -58.713980),
		new Ubicacion(-34.540564, -58.715350),
		new Ubicacion(-34.538690, -58.713434) 
	 ]
	 )
	,
	new Zona 
	 ( 
	 "blue",
	 [ 
	 new Ubicacion(-34.541501, -58.713980), 
		new Ubicacion(-34.541931, -58.714422), 
		new Ubicacion(-34.544664, -58.710663), 
		new Ubicacion(-34.544169, -58.710247),
		new Ubicacion(-34.544977, -58.709127),
		new Ubicacion(-34.546744, -58.711015),
		new Ubicacion(-34.542327, -58.717222),
		new Ubicacion(-34.540564, -58.715350),
		new Ubicacion(-34.541501, -58.713980) 

	 ]
	 )
	
	];

	
}


var LeafIcon;
var orangeIcon;
var redIcon;
var estiloPopup;

function cargarEstilos()
{
			document.getElementById("mapid").style.width = "75%"; 
			document.getElementById("right").style.width = "25%"; 
			document.getElementById("right").style.visibility = "visible"; 
	
	this.LeafIcon = L.Icon.extend(
	{
		options: 
		{
				shadowUrl:     'leaflet/images/marker-shadow.png',
				iconSize:    [25, 41],
				iconAnchor:  [12, 41],
				popupAnchor: [1, -34],
				tooltipAnchor: [16, -28],
				shadowSize:  [41, 41]
		}
	});	

	this.orangeIcon = new LeafIcon({iconUrl: 'leaflet/images/marker-orange-icon.png'});
	this.redIcon = new LeafIcon({iconUrl: 'leaflet/images/marker-red-icon.png'});

	L.icon = function (options) 
		{
			return new L.Icon(options);
		};
	
	this.estiloPopup = {'maxWidth': '300'};
}




